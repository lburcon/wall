
# Opis użytej architektury #
W aplikacji została użyta architektura MVVVM, która docelowo mogłaby zostać przeniesiona do współdzielonego kodu KMP, a wystawianie obserwowalnych danych następowałoby za pomocą Flow.
Starałem się wypracować dość podstawowy interfejs nawigacyjny używając Jetpack Navigation Componentu, dzięki czemu w łatwy sposób można dodawać nowe node'y nawigacyjne. Niestety libka od nawigacji jest jeszcze dość ograczona, np. brakuje animacji (są w osobnej Libce accompanist, której nie chciałem dodawać ze względu na czas)
Użyłem nowego Androidowego Splash Screena do pobrania początkowej listy obrazków, dlatego też nie ma żadnego loadingu etc. Wiem, że ikonka nie jest wycentrowana i się nie mieści, ale też nie chciałem na to tracić czasu.

# Opis niedokończonych zadań, które uznałem, że nie są corową funkcjonalnością aplikacji #

* Paging - zająłby zdecydowanie za dużo czasu z racji braku ustalonych standardów i dokumentacji od Google. Oczywiście paging jako taki w Compose istnieje, jednak są dwa różne sposoby (Paging3 API vs Accompanist).
* Brak cachowania - Gdybym musiał je zaimplementować to miałoby to miejsce w repozytorium, gdzie sprawdzałbym, czy dane nie istnieją w bazie danych Room.
* Z racji braku skomplikowanej logiki biznesowej ominąłem tworzenie UseCase'ów. Jedyne co by robiły, to wołały Repozytorium. W przypadku, gdyybm dodawał cachowanie, zdecydowanie stworzyłbym UseCase'y.
* Brak sprawdzania szerokości ekranu i dostosowywania grida - Koncept jest fajny, i dość prosty używając Compose, jednak zbrakło mi czasu i wolałem się skupić na architekturze.
* Widoki pozostawiają wiele do życzenia - nie miałem weny i czasu na tworzenie przyjaznego interfejsu :P Brak również placeholderów na obrazkach. Wystarczy użyć "Accompanist placeholder", ale brak czasu na dodanie dodatkowej libki.
* Pozostawiłem wiele TODO takich jak np. wyniesienie reużywalnych kontrolek do osobnego modułu, które przy tym rozmiarze aplikacji nie są wymagane, ale wraz z jej rozwojem byłyby wręcz konieczne.