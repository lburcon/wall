package burcon.lukasz.wall

import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isInstanceOf
import burcon.lukasz.wall.domain.api.PicturesApi
import burcon.lukasz.wall.domain.model.PictureApiModel
import burcon.lukasz.wall.domain.repository.PicturesRepository
import burcon.lukasz.wall.mapper.PictureListMapper
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class PicturesRepositoryTest {

    @RelaxedMockK
    lateinit var api: PicturesApi

    private val mapper = PictureListMapper()

    private lateinit var subject: PicturesRepository

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)

        subject = PicturesRepository(
            api = api,
            pictureListMapper = mapper
        )
    }

    @Test
    fun `success returns success model and data`() = runBlocking {
        coEvery { api.getPictureList() } returns listOf(
            PictureApiModel(
                id = "0",
                author = "author0",
                url = "url/to.image0"
            ),
            PictureApiModel(
                id = "1",
                author = "author1",
                url = "url/to.image1"
            ),
        )

        val result = subject.getPictureList()

        assertThat(result).isInstanceOf(PicturesRepository.Result.Success::class.java)
        assertThat((result as PicturesRepository.Result.Success).data.size).isEqualTo(2)
    }

    @Test
    fun `success returns error model`() = runBlocking {
        coEvery { api.getPictureList() } throws Exception()

        val result = subject.getPictureList()

        assertThat(result).isInstanceOf(PicturesRepository.Result.Error::class.java)
        assertThat(result).isEqualTo(PicturesRepository.Result.Error)
    }
}