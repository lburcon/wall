package burcon.lukasz.wall

import assertk.assertThat
import assertk.assertions.isEqualTo
import burcon.lukasz.wall.domain.repository.PicturesRepository
import burcon.lukasz.wall.domain.viewmodel.PictureListViewModel
import burcon.lukasz.wall.domain.viewmodel.UiState
import burcon.lukasz.wall.ui.model.PictureListModel
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class PictureListViewModelTest {

    @RelaxedMockK
    lateinit var repository: PicturesRepository

    private lateinit var subject: PictureListViewModel

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)

        subject = PictureListViewModel(
            repository = repository
        )
    }

    @Test
    fun `loadPicturesList() - result is propagated`() = runBlocking {
        coEvery { repository.getPictureList() } returns PicturesRepository.Result.Success(
            listOf(
                PictureListModel(
                    id = "0",
                    author = "author0",
                    url = "url/to.image0",
                    thumbnailUlr = "url/to.image.thumbnail0",
                ),
                PictureListModel(
                    id = "1",
                    author = "author1",
                    url = "url/to.image1",
                    thumbnailUlr = "url/to.image.thumbnail1",
                ),
            )
        )

        subject.loadPicturesList()

        val picturesList = subject.picturesList.value
        picturesList as UiState.Content

        assertThat(picturesList.data.size).isEqualTo(2)
        val firstPicture = picturesList.data.first()
        assertThat(firstPicture.id).isEqualTo("0")
        assertThat(firstPicture.author).isEqualTo("author0")
        assertThat(firstPicture.url).isEqualTo("url/to.image0")
        assertThat(firstPicture.thumbnailUlr).isEqualTo("url/to.image.thumbnail0")
        val secondPicture = picturesList.data[1]
        assertThat(secondPicture.id).isEqualTo("1")
        assertThat(secondPicture.author).isEqualTo("author1")
        assertThat(secondPicture.url).isEqualTo("url/to.image1")
        assertThat(secondPicture.thumbnailUlr).isEqualTo("url/to.image.thumbnail1")
    }


}