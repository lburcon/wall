package burcon.lukasz.wall

import assertk.assertThat
import assertk.assertions.isEqualTo
import burcon.lukasz.wall.domain.model.PictureApiModel
import burcon.lukasz.wall.mapper.PictureListMapper
import org.junit.Test

class PictureListMapperTest {

    private val subject = PictureListMapper()

    @Test
    fun `mapList() works correctly`() {
        val data = listOf(
            PictureApiModel(
                id = "0",
                author = "author0",
                url = "url/to.image0"
            ),
            PictureApiModel(
                id = "1",
                author = "author1",
                url = "url/to.image1"
            ),
        )

        val result = subject.mapList(data)

        assertThat(result.size).isEqualTo(2)
        val firstPicture = result.first()
        assertThat(firstPicture.id).isEqualTo("0")
        assertThat(firstPicture.author).isEqualTo("author0")
        assertThat(firstPicture.url).isEqualTo("url/to.image0")
        val secondPicture = result[1]
        assertThat(secondPicture.id).isEqualTo("1")
        assertThat(secondPicture.author).isEqualTo("author1")
        assertThat(secondPicture.url).isEqualTo("url/to.image1")
    }

    @Test
    fun `mapList() works correctly when empty list`() {
        val data = emptyList<PictureApiModel>()

        val result = subject.mapList(data)

        assertThat(result.size).isEqualTo(0)
    }

    @Test
    fun `mapItem() works correctly`() {
        val data = PictureApiModel(
            id = "0",
            author = "author0",
            url = "url/to.image0"
        )

        val result = subject.mapItem(data)

        assertThat(result.id).isEqualTo("0")
        assertThat(result.author).isEqualTo("author0")
        assertThat(result.url).isEqualTo("url/to.image0")
    }
}