package burcon.lukasz.wall

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import com.github.michaelbull.retry.policy.constantDelay
import com.github.michaelbull.retry.policy.limitAttempts
import com.github.michaelbull.retry.policy.plus
import com.github.michaelbull.retry.retry
import kotlinx.coroutines.runBlocking
import org.junit.Rule
import org.junit.Test

class PicturesTest {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    @Test
    fun pictureDetailIsDisplayed() {
        retryOnFail {
            composeTestRule.onNodeWithTag("picture_0").performClick()
        }

        retryOnFail {
            composeTestRule.onNodeWithTag("picture_0").assertIsDisplayed()
            composeTestRule.onNodeWithTag("author_title").assertIsDisplayed()
        }
    }

    @Test
    fun onlyInitialPicturesAreExisting() {
        retryOnFail {
            composeTestRule.onNodeWithTag("picture_29").assertDoesNotExist()
            composeTestRule.onNodeWithTag("picture_30").assertDoesNotExist()
        }
    }

    /**
     * Retries assertion up to 5 times if it was not successful the last time
     */
    private fun retryOnFail(delay: Long = 5_000L, invoke: () -> Unit) {

        runBlocking {
            retry(limitAttempts(5).plus(constantDelay(delay))) {
                invoke()
            }
        }
    }
}