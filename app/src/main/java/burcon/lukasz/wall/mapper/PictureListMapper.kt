package burcon.lukasz.wall.mapper

import burcon.lukasz.wall.domain.model.PictureApiModel
import burcon.lukasz.wall.ui.model.PictureListModel
import javax.inject.Inject

class PictureListMapper @Inject constructor() {

    companion object {
        private const val THUMBNAIL_IMAGE_WIDTH_PX = 1000
        private const val THUMBNAIL_IMAGE_HEIGHT_PX = 1000
    }

    fun mapList(data: List<PictureApiModel>): List<PictureListModel> {
        return data.map {
            mapItem(it)
        }
    }

    fun mapItem(data: PictureApiModel): PictureListModel {
        return PictureListModel(
            id = data.id,
            author = data.author,
            url = data.url,
            thumbnailUlr = createSmallPictureUrl(data.id)
        )
    }

    private fun createSmallPictureUrl(id: String): String {
        return "https://picsum.photos/id/$id/$THUMBNAIL_IMAGE_WIDTH_PX/$THUMBNAIL_IMAGE_HEIGHT_PX"
    }
}