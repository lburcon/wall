package burcon.lukasz.wall.domain.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class PictureApiModel(
    val id: String,
    val author: String,
    @SerialName("download_url")
    val url: String,
)
