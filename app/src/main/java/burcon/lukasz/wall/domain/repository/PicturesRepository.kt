package burcon.lukasz.wall.domain.repository

import android.util.Log
import burcon.lukasz.wall.domain.api.PicturesApi
import burcon.lukasz.wall.mapper.PictureListMapper
import burcon.lukasz.wall.ui.model.PictureListModel
import javax.inject.Inject

class PicturesRepository @Inject constructor(
    private val api: PicturesApi,
    private val pictureListMapper: PictureListMapper
) {

    // TODO add basic cache

    suspend fun getPictureList(): Result<List<PictureListModel>> {
        return try {
            val result = api.getPictureList()

            Result.Success(pictureListMapper.mapList(result))
        } catch (e: Exception) {
            Log.e("PictureRepository", e.message.toString())
            Result.Error
        }
    }

    suspend fun getPictureDetail(id: String): Result<PictureListModel> {
        return try {
            val result = api.getPictureDetail(id)

            Result.Success(pictureListMapper.mapItem(result))
        } catch (e: Exception) {
            Log.e("PictureRepository", e.message.toString())
            Result.Error
        }
    }

    sealed class Result<out T> {
        class Success<T>(val data: T) : Result<T>()
        object Error : Result<Nothing>()
    }
}