package burcon.lukasz.wall.domain.viewmodel

sealed class UiState<out T> {
    object Loading : UiState<Nothing>()
    class Content<T>(val data: T) : UiState<T>()
    object Error : UiState<Nothing>()
}