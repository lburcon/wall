package burcon.lukasz.wall.domain.viewmodel

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import burcon.lukasz.wall.domain.repository.PicturesRepository
import burcon.lukasz.wall.ui.model.PictureListModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.annotation.meta.Exhaustive
import javax.inject.Inject

@HiltViewModel
class PictureListViewModel @Inject constructor(
    private val repository: PicturesRepository
) : ViewModel() {

    private val picturesList_ = mutableStateOf<UiState<List<PictureListModel>>>(UiState.Loading)
    val picturesList: State<UiState<List<PictureListModel>>> = picturesList_

    suspend fun loadPicturesList() {
        val result = repository.getPictureList()

        @Exhaustive
        when (result) {
            PicturesRepository.Result.Error -> {
                picturesList_.value = UiState.Error
            }
            is PicturesRepository.Result.Success -> {
                picturesList_.value = UiState.Content(result.data)
            }
        }
    }
}