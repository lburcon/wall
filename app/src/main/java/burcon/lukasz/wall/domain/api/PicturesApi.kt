package burcon.lukasz.wall.domain.api

import burcon.lukasz.wall.domain.model.PictureApiModel
import retrofit2.http.GET
import retrofit2.http.Path

interface PicturesApi {

    @GET("/v2/list")
    suspend fun getPictureList(): List<PictureApiModel>

    @GET("/id/{id}/info")
    suspend fun getPictureDetail(@Path("id") id: String): PictureApiModel
}