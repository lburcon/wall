package burcon.lukasz.wall.domain.viewmodel

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import burcon.lukasz.wall.domain.repository.PicturesRepository
import burcon.lukasz.wall.ui.model.PictureListModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.annotation.meta.Exhaustive
import javax.inject.Inject

@HiltViewModel
class PictureDetailViewModel @Inject constructor(
    private val repository: PicturesRepository
) : ViewModel() {

    private val pictureDetail_ = mutableStateOf<UiState<PictureListModel>>(UiState.Loading)
    val pictureDetail: State<UiState<PictureListModel>> = pictureDetail_

    suspend fun loadPictureDetail(id: String) {
        val result = repository.getPictureDetail(id)
        @Exhaustive
        when (result) {
            PicturesRepository.Result.Error -> {
                pictureDetail_.value = UiState.Error
            }
            is PicturesRepository.Result.Success -> {
                pictureDetail_.value = UiState.Content(result.data)
            }
        }
    }
}