package burcon.lukasz.wall

import android.content.Context
import android.content.Intent

fun Context.sharePictureUrl(url: String) {
    val intent = Intent.createChooser(
        Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, url)
            type = "text/plain"
        },
        null
    )
    startActivity(intent)
}
