package burcon.lukasz.wall.ui.screen

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.unit.dp
import burcon.lukasz.wall.ui.model.PictureListModel
import coil.compose.rememberImagePainter

private val State = object {
    val grilCellsCount = 2
    val pictureSize = 128.dp
    val pictureBorderWidth = 1.dp
    val pictureBorderColor = Color.Black
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun PictureListScreen(
    pictures: List<PictureListModel>,
    navigateToPictureDetails: (id: String) -> Unit
) {
    LazyVerticalGrid(
        modifier = Modifier.fillMaxWidth(),
        cells = GridCells.Fixed(State.grilCellsCount)
    ) {
        items(pictures.size) { index ->
            PictureItem(
                model = pictures[index],
                onClick = navigateToPictureDetails
            )
        }
    }
}

// TODO should be in a separate, widget related, module
@Composable
private fun PictureItem(model: PictureListModel, onClick: (id: String) -> Unit) {
    Image(
        modifier = Modifier
            .size(State.pictureSize)
            .border(State.pictureBorderWidth, State.pictureBorderColor)
            .clickable {
                onClick(model.id)
            }
            .testTag("picture_${model.id}"),
        painter = rememberImagePainter(model.thumbnailUlr),
        contentDescription = null,
        contentScale = ContentScale.FillBounds
    )
}