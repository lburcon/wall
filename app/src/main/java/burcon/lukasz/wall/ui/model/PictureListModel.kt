package burcon.lukasz.wall.ui.model

data class PictureListModel(
    val id: String,
    val author: String,
    val url: String,
    val thumbnailUlr: String,
)