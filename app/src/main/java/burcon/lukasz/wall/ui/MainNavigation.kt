package burcon.lukasz.wall.ui

import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import burcon.lukasz.wall.domain.viewmodel.PictureDetailViewModel
import burcon.lukasz.wall.ui.model.PictureListModel
import burcon.lukasz.wall.ui.screen.PictureDetailScreen
import burcon.lukasz.wall.ui.screen.PictureListScreen

sealed class Routing(val route: String) {
    object PictureList : Routing("picturesList")
    object PictureDetails : Routing("pictureDetail/{id}") {
        fun createRoute(id: String) = "pictureDetail/$id"
    }
}

@Composable
fun MainNavigation(pictures: List<PictureListModel>) {
    val navController = rememberNavController()

    NavHost(navController = navController, startDestination = Routing.PictureList.route) {
        addPictureList(navController, pictures)
        addPictureDetails()
    }
}

private fun NavGraphBuilder.addPictureList(
    navController: NavHostController,
    pictures: List<PictureListModel>
) {
    composable(Routing.PictureList.route) {
        PictureListScreen(
            pictures = pictures,
            navigateToPictureDetails = { id ->
                navController.navigate(Routing.PictureDetails.createRoute(id))
            }
        )
    }
}

private fun NavGraphBuilder.addPictureDetails() {
    composable(Routing.PictureDetails.route) { entry ->
        val detailViewModel = hiltViewModel<PictureDetailViewModel>()

        PictureDetailScreen(
            id = requireNotNull(entry.arguments?.getString("id")),
            detailViewModel = detailViewModel
        )
    }
}
