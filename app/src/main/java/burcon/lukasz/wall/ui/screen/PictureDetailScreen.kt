package burcon.lukasz.wall.ui.screen

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Button
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import burcon.lukasz.wall.domain.viewmodel.PictureDetailViewModel
import burcon.lukasz.wall.domain.viewmodel.UiState
import burcon.lukasz.wall.sharePictureUrl
import burcon.lukasz.wall.ui.model.PictureListModel
import coil.compose.rememberImagePainter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@Composable
fun PictureDetailScreen(id: String, detailViewModel: PictureDetailViewModel) {
    InitialLoadData(id, detailViewModel)

    when (val detailModel = detailViewModel.pictureDetail.value) {
        is UiState.Content -> {
            ScreenContent(detailModel.data)
        }
        UiState.Error -> {
            val coroutineScope = rememberCoroutineScope()

            ErrorContent(
                onRetry = {
                    coroutineScope.launch {
                        withContext(Dispatchers.IO) {
                            detailViewModel.loadPictureDetail(id)
                        }
                    }
                }
            )
        }
        UiState.Loading -> {
            LoadingContent()
        }
    }
}

@Composable
private fun ScreenContent(model: PictureListModel) {
    val context = LocalContext.current

    Column(
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            modifier = Modifier
                .weight(5f)
                .testTag("picture_${model.id}"),
            painter = rememberImagePainter(model.url),
            contentDescription = null,
            contentScale = ContentScale.FillBounds
        )

        Text(
            modifier = Modifier.testTag("author_title"),
            text = model.author
        )

        Button(
            onClick = { context.sharePictureUrl(model.url) }
        ) {
            Text(
                text = "Share"
            )
        }
    }
}

@Composable
private fun ErrorContent(onRetry: () -> Unit) {
    // TODO Make error view a little bit more appealing

    TextButton(
        modifier = Modifier.fillMaxWidth(),
        onClick = onRetry
    ) {
        Text(
            text = "Retry"
        )
    }
}

@Composable
private fun LoadingContent() {
    Box(
        modifier = Modifier.fillMaxSize()
    ) {
        CircularProgressIndicator(
            modifier = Modifier.align(Alignment.Center)
        )
    }
}

@Composable
private fun InitialLoadData(id: String, detailViewModel: PictureDetailViewModel) {
    LaunchedEffect(id) {
        launch {
            withContext(Dispatchers.IO) {
                detailViewModel.loadPictureDetail(id)
            }
        }
    }
}
