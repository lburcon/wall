package burcon.lukasz.wall

import android.os.Bundle
import android.view.ViewTreeObserver.OnPreDrawListener
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalView
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import burcon.lukasz.wall.domain.viewmodel.PictureListViewModel
import burcon.lukasz.wall.domain.viewmodel.UiState
import burcon.lukasz.wall.ui.MainNavigation
import burcon.lukasz.wall.ui.theme.WallTheme
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    val viewModel: PictureListViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        installSplashScreen()

        setContent {
            HandleSplashScreen()
            InitialLoadData()

            WallTheme {
                Surface(color = MaterialTheme.colors.background) {
                    MainView()
                }
            }
        }
    }

    @Composable
    private fun MainView() {
        when (val picturesState = viewModel.picturesList.value) {
            is UiState.Content -> {
                MainNavigation(pictures = picturesState.data)
            }
            UiState.Error -> {
                val coroutineScope = rememberCoroutineScope()
                SplashScreenError(
                    onRetry = {
                        coroutineScope.launch {
                            withContext(Dispatchers.IO) {
                                viewModel.loadPicturesList()
                            }
                        }
                    }
                )
            }
            UiState.Loading -> {
                // no-op, splash screen is being shown
            }
        }
    }

    @Composable
    fun SplashScreenError(onRetry: () -> Unit) {
        // TODO Make error view a little bit more appealing

        TextButton(
            modifier = Modifier.fillMaxWidth(),
            onClick = onRetry
        ) {
            Text(
                text = "Retry"
            )
        }
    }

    @Composable
    private fun InitialLoadData() {
        LaunchedEffect(Unit) {
            launch {
                withContext(Dispatchers.IO) {
                    viewModel.loadPicturesList()
                }
            }
        }
    }

    /**
     * Waits for external data downloading and closes splash screen on resut
     */
    @Composable
    private fun HandleSplashScreen() {
        val viewLocal = LocalView.current

        viewLocal.viewTreeObserver.addOnPreDrawListener(
            object : OnPreDrawListener {
                override fun onPreDraw(): Boolean {
                    // Check if the initial data is ready.
                    return if (viewModel.picturesList.value != UiState.Loading) {
                        viewLocal.viewTreeObserver.removeOnPreDrawListener(this)
                        true
                    } else {
                        false
                    }
                }
            }
        )
    }
}
